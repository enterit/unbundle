import test from 'ava'
import {parse} from '../lib/argv'
import {resolve} from 'path'

test('returns parsed options', (t) => {
  const fixture = '--silent --entry foo.js --destination bar/baz'.split(' ')
  const expected = {
    silent: true,
    entry: 'foo.js',
    destination: 'bar/baz'
  }
  const output = parse(fixture)
  for (const key of Object.keys(expected)) {
    t.is(output[key], expected[key])
  }
})

test('auto-detect entry', (t) => {
  const output = parse([])
  t.is(output.entry, resolve(__dirname, '../lib/Runner.js'))
})

test('auto-detect destination', (t) => {
  const output = parse([])
  t.is(output.destination, resolve('public'))
})
