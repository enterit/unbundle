import {hash64} from 'xxhash'

export class Fingerprinter {
  constructor () {
    this.fingerprints = Object.create(null)
  }

  hash ({location, data}) {
    if (!(location in this.fingerprints)) {
      const fingerprint = hash64(data, 0)
      this.fingerprints[location] = fingerprint
    }
    return this.fingerprints[location]
  }

  expire ({location}) {
    if (Object.prototype.hasOwnProperty.call(this.fingerprints, location)) {
      delete this.fingerprints[location]
    } else {
      throw Error(`Not found in fingerprints cache: ${location}`)
    }
  }
}
