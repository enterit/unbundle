import debug from 'debug'
import {parse} from './argv'
import {Runner} from './Runner'

const argv = parse(process.argv.slice(2))

if (!argv.silent) {
  debug.enable('runner')
}

const runner = new Runner(argv.entry, argv.destination)
if (argv.watch) {
  process.on('SIGINT', () => {
    console.log('')
    runner.kill()
  })
  runner.watch()
} else {
  runner
    .run()
    .then(::runner.kill)
}
