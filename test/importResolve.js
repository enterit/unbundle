import test from 'ava'
import {importResolve} from '../lib/importResolve'

const fixtures = {
  'prefix current path (./)': {
    file: {
      outputPath: '/foobar/public/app.js'
    },
    assets: [
      {
        location: '/foobar/node_modules/react/react.js',
        outputPath: '/foobar/public/node_modules/react/react.js'
      }
    ],

    basedir: undefined,
    dependency: '/foobar/node_modules/react/react.js',
    source: undefined,
    filename: undefined,

    expected: './node_modules/react/react.js'
  },

  'no prefix for ancestor paths (../)': {
    file: {
      outputPath: '/foobar/public/stuff/foo.js'
    },
    assets: [
      {
        location: '/foobar/source/components/MyComponent.js',
        outputPath: '/foobar/public/components/MyComponent.js'
      }
    ],

    basedir: undefined,
    dependency: '/foobar/source/components/MyComponent.js',
    source: undefined,
    filename: undefined,

    expected: '../components/MyComponent.js'
  },

  'missing dependency': {
    type: 'throws',

    file: {
      outputPath: '/foobar/public/app.js'
    },
    assets: [],

    basedir: undefined,
    dependency: '/foobar/node_modules/nothing-here/index.js',
    source: 'nothing-here',
    filename: undefined,

    expected: 'Missing dependency: nothing-here'
  }
}

for (const [message, {
  type,
  file, assets,
  basedir, dependency, source, filename,
  expected
}] of Object.entries(fixtures)) {
  test(message, (t) => {
    if (!type || type === 'is') {
      const actual = importResolve(file, assets)(basedir, dependency, source, filename)
      t.is(actual, expected)
    } else if (type === 'throws') {
      t.throws(
        () => importResolve(file, assets)(basedir, dependency, source, filename),
        expected
      )
    } else {
      t.fail('Broken test case')
    }
  })
}
