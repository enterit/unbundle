#!/usr/bin/env node

'use strict'

const fs = require('fs')
const path = require('path')

const cli = path.join(__dirname, 'lib', 'cli.js')

if (fs.existsSync(cli)) {
  require(cli)
} else {
  console.error('Missing runtime files. Perhaps the postinstall script did not run.')
  process.exit(1)
}
