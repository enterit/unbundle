import Deps from 'module-deps'
import streamToPromise from 'stream-to-promise'
import ora from 'ora'
import {fileCache} from './fileCache'
import isBuiltinModule from 'is-builtin-module'

async function twiddle (promise, options) {
  const spinner = ora(options).start()
  promise.then(() => spinner.stop().clear())
  return await promise
}

// Monkey-patch for fileCache
// PR: https://github.com/substack/module-deps/pull/121
Deps.prototype.__readFile = Deps.prototype.readFile
Deps.prototype.readFile = function (file, id, pkg) {
  if (isBuiltinModule(id)) throw new Error(`Built-in Node.js modules are not supported. Module: '${id}'`)
  const hop = Object.prototype.hasOwnProperty
  if (this.fileCache::hop(file)) {
    var c = this.fileCache[file]
    if (typeof c.pipe === 'function') return c
  }
  return this.__readFile(...arguments)
}

export class Tracer {
  constructor (options) {
    this.options = Object.assign({
      ignoreMissing: true,
      cache: Object.create(null),
      packageCache: Object.create(null),
      fileCache: fileCache(),
      transform: [
        [
          require.resolve('babelify'), {
            babelrc: false,
            presets: [
              [
                require.resolve('babel-preset-env'),
                {
                  targets: {
                    node: 'current'
                  }
                }
              ],
              require.resolve('babel-preset-stage-0'),
              require.resolve('babel-preset-react')
            ]
          }
        ]
      ]
    }, options)
  }

  async trace (entry) {
    const search = new Deps(this.options)
    search.end({file: entry})
    const dependencies = await twiddle(
      streamToPromise(search),
      {text: 'Tracing dependencies'}
    )
    return dependencies
      .map(({file: location}) => location)
      .map((location) => ({
        location,
        data: this.options.fileCache[location]
      }))
  }

  expire (location) {
    delete this.options.cache[location]
    delete this.options.packageCache[location]
    delete this.options.fileCache[location]
  }
}
