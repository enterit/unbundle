import {basename} from 'path'
import write from 'write'
import promisify from 'pify'
// import {parse} from 'babylon'
// import {transformFromAst} from 'babel-core'
import {transform} from 'babel-core'
import {importResolve} from './importResolve'

const babelOptions = {
  babelrc: false,
  comments: false,
  minified: true,
  sourceMaps: true
}

// const babylonPlugins = [
//   'jsx',
//   'flow',
//   'doExpressions',
//   'objectRestSpread',
//   'decorators',
//   'classProperties',
//   'exportExtensions',
//   'asyncGenerators',
//   'functionBind',
//   'functionSent',
//   'dynamicImport'
// ]

const babelPlugins = [
  'babel-plugin-syntax-async-generators',
  'babel-plugin-syntax-class-properties',
  'babel-plugin-syntax-decorators',
  'babel-plugin-syntax-do-expressions',
  'babel-plugin-syntax-export-extensions',
  'babel-plugin-syntax-function-bind',
  'babel-plugin-syntax-function-sent',
  'babel-plugin-syntax-object-rest-spread'
].map((plugin) => require(plugin))

const babelPresets = [
]

function babelrc (options = {}, plugins = [], presets = []) {
  const result = Object.assign({}, babelOptions, options)
  result.plugins = babelPlugins.concat(plugins)
  result.presets = babelPresets.concat(presets)
  return result
}

const stages = (file, assets) => [
  {
    // Inline NODE_ENV ("development"/"production")
    // Must run before minify-dead-code-elimination
    plugins: [require('babel-plugin-transform-node-env-inline')],
    // plugins: ['transform-inline-environment-variables'],

    // Parse and transform JSX/Flow
    presets: [require('babel-preset-react')]
  },

  // Get rid of require() calls that are not used in the current prod/dev environment
  {plugins: [require('babel-plugin-minify-dead-code-elimination')]},

  // Transform CommonJS to ES2015 modules
  {plugins: [require('babel-plugin-transform-commonjs-es2015-modules')]},

  // Trace all import sources
  {plugins: [[require('babel-plugin-transform-import-resolve'), {parse: importResolve(file, assets)}]]},

  // Transform ES2015 modules to SystemJS for client-side asynchronous loading
  {plugins: [require('babel-plugin-transform-es2015-modules-systemjs')]},

  // Transform to minified ES5 code
  {presets: [require('babel-preset-latest'), require('babel-preset-stage-0'), require('babel-preset-babili')]}
]

export async function processFile ({location, data, outputPath}, assets) {
  let code = data
  let map = null
  // let ast = parse(code, {
  //   sourceFilename: basename(location),
  //   sourceType: 'module',
  //   plugins: babylonPlugins
  // })

  for (const {plugins, presets} of stages({location, outputPath}, assets)) {
    // ({ast, code, map} = transformFromAst(ast, code, babelrc({
    ({code, map} = transform(code, babelrc({
      filename: location,
      inputSourceMap: map
    }, plugins, presets)))
  }

  code = code + '\n//# sourceMappingURL=' + encodeURI(basename(outputPath)) + '.map'

  await Promise.all([
    promisify(write)(outputPath, code),
    promisify(write)(outputPath + '.map', JSON.stringify(map))
  ])
}
