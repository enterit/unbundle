import React from 'react'
import ReactDOM from 'react-dom'
import MyComponent from './components/MyComponent'

console.log('🦄 Hello, World!')

ReactDOM.render(<MyComponent />, document.querySelector('main'))
