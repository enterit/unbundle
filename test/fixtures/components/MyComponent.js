import React from 'react'

class MyComponent extends React.Component {
  render () {
    return <h1>👋 Hello, World!</h1>
  }

  componentDidMount () {
    console.log('🤖 Beepbeep... MyComponent has mounted')
  }
}

export default MyComponent
