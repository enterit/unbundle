import {dirname, relative} from 'path'
import slash from 'slash'

class DependencyError extends Error {
  type = 'DependencyError'
}

export const importResolve = (file, assets) => (basedir, dependency, source, filename) => {
  const asset = assets.find(({location}) => location === dependency)
  if (!asset) throw new DependencyError(`Missing dependency: ${source}`)
  let result = relative(dirname(file.outputPath), asset.outputPath)

  if (!result.startsWith('.')) {
    result = './' + result
  }

  return slash(result)
}
