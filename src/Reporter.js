export class Reporter /* extends Promise */ {
  progress = 0

  constructor (promises, options) {
    // super()
    this.total = promises.length || promises.size
    this._promise = new Promise((resolve, reject) => {
      this.resolve = resolve
      this.reject = reject
    })
    for (const promise of promises) {
      promise.then(
        (res) => this.resolved(promise, res),
        (err) => this.rejected(promise, err)
      )
    }
  }

  then (...args) {
    this._promise.then(...args)
  }

  catch (...args) {
    this._promise.catch(...args)
  }

  resolved (promise, res) {
    this.progress += 1
    if (this.progress === this.total) this.resolve(this.promises)
  }

  rejected (promise, err) {
    this.reject(err)
  }
}
