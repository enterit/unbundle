import {createReadStream} from 'fs'
import through from 'through2'
import concat from 'concat-stream'

export function fileCache () {
  const pending = Object.create(null)

  return new Proxy(Object.create(null), {
    get (target, property) {
      if (property in target) return target[property]
      if (property in pending) {
        const stream = through()
        pending[property].push(stream)
        return stream
      }
    },

    getOwnPropertyDescriptor (target, property) {
      if (!(property in target) && !(property in pending)) {
        const file = createReadStream(property) // , {encoding: 'utf8'})
        file.on('error', (error) => { throw error })
        const slurp = concat((data) => {
          target[property] = data
          for (const stream of pending[property]) {
            stream.end(data)
          }
          delete pending[property]
        })
        file.pipe(slurp)
        pending[property] = []
        return Object.getOwnPropertyDescriptor(pending, property)
      }
      return Object.getOwnPropertyDescriptor(target, property)
    }
  })
}
