import test from 'ava'
import {all} from 'pokemon'

import {Fingerprinter} from '../lib/Fingerprinter'

test('Caching helps performance', async (t) => {
  const fingerprinter = new Fingerprinter()

  const fixtures = []
  for (const pokemon of all()) {
    const fixture = {
      location: pokemon,
      data: Buffer.from(pokemon.repeat(10000))
    }
    fixtures.push(fixture)
  }

  function toMilliseconds ([seconds, nanoseconds]) {
    return ((seconds * 1e9) + nanoseconds) / 1e6
  }

  const beforeUncached = process.hrtime()
  for (const fixture of fixtures) fingerprinter.hash(fixture)
  const uncached = toMilliseconds(process.hrtime(beforeUncached))

  const beforeCached = process.hrtime()
  for (const fixture of fixtures) fingerprinter.hash(fixture)
  const cached = toMilliseconds(process.hrtime(beforeCached))

  t.true(cached < uncached, 'cached time is faster')
})

test('Cache expiration', (t) => {
  const first = {location: 'foo', data: new Buffer([1])}
  const second = {location: 'foo', data: new Buffer([2])}

  const fingerprinter = new Fingerprinter()

  const firstHash = fingerprinter.hash(first)
  const firstHashRetry = fingerprinter.hash(first)
  const firstHashFake = fingerprinter.hash(second)
  fingerprinter.expire(first)
  const secondHash = fingerprinter.hash(second)

  t.is(firstHash, firstHashRetry)
  t.is(firstHash, firstHashFake)
  t.not(firstHash, secondHash)
})

test('Invalid cache expiration', (t) => {
  const fingerprinter = new Fingerprinter()
  t.throws(
    () => fingerprinter.expire({location: 'foo'}),
    'Not found in fingerprints cache: foo'
  )
})
