import {relative} from 'path'
import debug from 'debug'
import pick from 'lodash.pick'
import {reporter} from './Progress'
import {Pool} from './workers'

export class Pipeline {
  constructor () {
    this.pool = new Pool()
  }

  async process (files, cached = files) {
    const slimFiles = files.map((file) => {
      const slimFile = pick(file, ['location', 'outputPath'])
      slimFile.data = file.data.toString('utf8')
      return slimFile
    })

    const slimCached = cached
      .map((file) => pick(file, ['location', 'outputPath']))

    const jobs = this.pool.process(slimFiles, slimCached)
      .map((job) => job.then((file) => relative(process.cwd(), file.location)))

    await reporter(jobs, {log: debug('pipeline')})
  }

  kill () {
    this.pool.kill()
  }
}
