import yargs from 'yargs'
import {resolve} from 'path'
import {sync as pkgDir} from 'pkg-dir'
import {sync as resolveJS} from 'resolve'

export function parse (input) {
  const argv = yargs
    .usage('Usage: $0 [options]')

    .example('$0 -i src/app.js -o dist', 'Transpiles the app and its dependencies into the "dist" directory.')

    .alias('i', 'entry')
    .describe('entry', 'Starting file for the application. Defaults to package.json "main" field.')

    .alias('o', 'destination')
    .describe('destination', 'Directory to write output. Defaults to "./public". Nonexistent directories are automatically created.')

    .alias('s', 'silent')
    .boolean('silent')
    .default('silent', false)
    .describe('silent', 'Suppress log messages from output')

    .alias('w', 'watch')
    .boolean('watch')
    .default('watch', false)
    .describe('watch', 'Monitor files and rebuild on changes')

    .alias('h', 'help')
    .help('help')

    .alias('v', 'version')
    .version()

    .parse(input)

  if (!argv.entry) {
    argv.entry = resolveJS(pkgDir())
  }

  if (!argv.destination) {
    argv.destination = resolve(process.cwd(), 'public')
  }

  return argv
}
