import isCi from 'is-ci'
import {Verbose} from './Verbose'
import {Gauge} from './Gauge'

export async function reporter (promises, options) {
  const Reporter = isCi ? Verbose : Gauge
  const reporter = new Reporter(promises, options)
  return await reporter
}
