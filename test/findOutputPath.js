import test from 'ava'
import {findOutputPath} from '../lib/findOutputPath'

const fixtures = {
  'relative path': {
    location: '/foo.js',
    fingerprint: new Buffer([0, 1, 2, 3, 4, 5, 6, 7, 8]),
    entry: '/bar.js',
    destination: '/dest',
    expected: '/dest/foo.😹.js'
  },
  'subdirectories': {
    location: '/foo/bar/abc.js',
    fingerprint: new Buffer([0, 1, 2, 3, 4, 5, 6, 7, 8]),
    entry: '/foo/def.js',
    destination: '/xyz',
    expected: '/xyz/bar/abc.😹.js'
  },
  'move node_modules to top of the output tree': {
    location: '/foo/node_modules/def/xyz.js',
    fingerprint: new Buffer([0, 1, 2, 3, 4, 5, 6, 7, 8]),
    entry: '/foo/bar/abc.js',
    destination: '/foo/vuw',
    expected: '/foo/vuw/node_modules/def/xyz.😹.js'
  },
  'only cut off one of the nested node_modules': {
    location: '/foo/node_modules/def/node_modules/xyz/vuw.js',
    fingerprint: new Buffer([0, 1, 2, 3, 4, 5, 6, 7, 8]),
    entry: '/foo/node_modules/abc.js',
    destination: '/ghi',
    expected: '/ghi/node_modules/xyz/vuw.😹.js'
  },
  'contain directories within entry point base directory': {
    location: '/foo/bar.js',
    fingerprint: new Buffer([0, 1, 2, 3, 4, 5, 6, 7, 8]),
    entry: '/baz/abc.js',
    destination: '/dest',
    expected: '/dest/👀/foo/bar.😹.js'
  },
  'allow multiple levels up from entry': {
    location: '/foo/bar.js',
    fingerprint: new Buffer([0, 1, 2, 3, 4, 5, 6, 7, 8]),
    entry: '/baz/abc/def/ghi.js',
    destination: '/dest',
    expected: '/dest/👀/👀/👀/foo/bar.😹.js'
  }
}

for (const scenario of Object.keys(fixtures)) {
  const {location, fingerprint, entry, destination, expected} = fixtures[scenario]
  test(scenario, (t) => {
    const output = findOutputPath({location, fingerprint}, entry, destination)
    t.is(output, expected)
  })
}
