import {resolve, dirname} from 'path'
import {unlink} from 'fs'
import debug from 'debug'
import promisify from 'pify'
import prunes from 'prunes'
import without from 'lodash.without'
import {Loader} from './Loader'
import {findOutputPath} from './findOutputPath'
import {Tracer} from './Tracer'
import {Watcher} from './Watcher'
import {Pipeline} from './Pipeline'
import {Fingerprinter} from './Fingerprinter'

export class Runner {
  constructor (entry, destination) {
    this.log = debug('runner')

    this.entry = resolve(process.cwd(), entry)
    this.destination = resolve(process.cwd(), destination)

    this.fingerprinter = new Fingerprinter()
    this.tracer = new Tracer()
    this.pipeline = new Pipeline()
    this.watcher = new Watcher()
    this.loader = new Loader()
  }

  async _trace () {
    return (await this.tracer.trace(this.entry))
      .map((asset) => Object.assign(asset, {
        fingerprint: this.fingerprinter.hash(asset)
      }))
      .map((asset) => Object.assign(asset, {
        outputPath: findOutputPath(asset, this.entry, this.destination)
      }))
  }

  async run () {
    const assets = await this._trace()
    this.loader.run(this.entry, this.destination, assets)
    await this.pipeline.process(assets)
    return assets
  }

  async watch () {
    this.watcher.kill()

    let assets = await this.run()
    this.watcher.add(assets)
    this.log(`🔍  Watching ${assets.length} source file${assets.length === 1 ? '' : 's'}. Press Ctrl+C to exit.`)

    this.watcher.on('change', async (events) => {
      const changed = events.map(([location]) => location)

      for (const location of changed) {
        this.tracer.expire(location)
        this.fingerprinter.expire({location})
      }

      const previous = assets.map(({location}) => location)
      const previousToOutputPath = assets
        .reduce((map, {location, outputPath}) => map.set(location, outputPath), new Map())
      assets = await this._trace()
      const updated = assets.map(({location}) => location)

      const added = without(updated, ...previous)
      const removed = without(previous, ...updated)

      this.watcher.add(added)
      this.watcher.unwatch(removed)

      for (const location of removed) {
        this.tracer.expire(location)
        this.fingerprinter.expire({location})
      }

      const deprecated = [...changed, ...removed]
        .map((location) => previousToOutputPath.get(location))
      if (deprecated.length > 0) this.log(`💥  Removing ${deprecated.length * 2} files`)
      await Promise.all(
        deprecated
          .map((outputPath) => Promise.all([
            promisify(unlink)(outputPath),
            promisify(unlink)(outputPath + '.map')
          ]))
      )
      await prunes(...deprecated.map((location) => dirname(location)))

      const expired = [...changed, ...added]
        .map((location) => assets.find((asset) => asset.location === location))

      if (changed.find((location) => location === this.entry)) {
        this.log(`💠  Rewiring entry point`)
        const old = previousToOutputPath.get(this.entry)
        const asset = assets.find((asset) => asset.location === this.entry)
        this.loader.expire(old, asset)
      }

      if (expired.length > 0) this.log(`✨  Transpiling ${expired.length * 2} files...`)
      await this.pipeline.process(expired, assets)

      this.log('🏁  Finished')
      this.log(`🔍  Watching ${assets.length} source file${assets.length === 1 ? '' : 's'}. Press Ctrl+C to exit.`)
    })
  }

  kill () {
    this.watcher.kill()
    this.pipeline.kill()
  }
}

// process.on('unhandledRejection', console.trace)
