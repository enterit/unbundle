import {basename, extname, resolve, relative, dirname, join} from 'path'
import fromBefore from 'from-before'
import {encode} from 'base-emoji-512'

export function findOutputPath ({location, fingerprint}, entry, destination) {
  const extension = extname(location)
  const filename = basename(location, extension)
  const [tag] = encode(fingerprint.slice(0, 2))
  const revved = filename + '.' + tag + extension

  const relativeToEntry = relative(dirname(entry), location)
  const moveNodeModules = relativeToEntry::fromBefore('node_modules/')
  const containedByEntry = moveNodeModules.replace(/\.\.\//g, '👀/')
  const absolutePath = resolve(destination, containedByEntry)
  const outdir = dirname(absolutePath)

  const target = join(outdir, revved)
  return target
}
