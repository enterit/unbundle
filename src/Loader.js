import {basename, dirname, join, relative, sep as separator} from 'path'
import {readFile} from 'fs'
import glob from 'glob'
import promisify from 'pify'
import {transform} from 'babel-core'
import write from 'write'
import {Fingerprinter} from './Fingerprinter'
import {findOutputPath} from './findOutputPath'
import slash from 'slash'

async function setupServiceWorkerImmutable () {
  const outputDirectory = join(this.destination, '💠', 'service-worker')

  let entry
  const main = require.resolve('cache-digest-immutable')
  const sourceDirectory = dirname(main)
  const filenames = await promisify(glob)('*.js', {cwd: sourceDirectory})

  const assets = []
  const sourceToDestination = new Map()

  for (const filename of filenames) {
    const location = join(sourceDirectory, filename)
    const data = await promisify(readFile)(location)
    const fingerprint = this.fingerprinter.hash({location, data})
    const outputPath = findOutputPath(
      {location, fingerprint},
      main,
      outputDirectory
    )
    const asset = {location, outputPath, fingerprint, data}
    if (location === main) entry = asset
    assets.push(asset)
    sourceToDestination.set(location, outputPath)
  }

  for (const {location, data, outputPath} of assets) {
    const options = {
      babelrc: false,
      comments: false,
      filename: location,
      minified: true,
      sourceMaps: true,
      plugins: [
        require('babel-plugin-transform-exponentiation-operator'),
        require('babel-plugin-transform-async-to-generator'),
        [require('babel-plugin-transform-import-scripts-resolve'), {outputDirectory, sourceToDestination}]
      ],
      presets: [
        // See: https://jakearchibald.github.io/isserviceworkerready/#navigator.serviceworker
        // ['env', {chrome: 40, firefox: 44}],
        require('babel-preset-babili')
      ]
    }

    const {code, map} = transform(data, options)
    await Promise.all([
      promisify(write)(
        outputPath,
        code + `\n//# sourceMappingURL=${encodeURI(basename(outputPath))}.map`
      ),
      promisify(write)(
        `${outputPath}.map`,
        JSON.stringify(map)
      )
    ])
  }

  return entry
}

async function setupSystemJs () {
  const outputDirectory = join(this.destination, '💠')

  const location = require.resolve('system-register-loader')
  const data = await promisify(readFile)(location)
  const fingerprint = this.fingerprinter.hash({location, data})
  const outputPath = findOutputPath(
    {location, fingerprint},
    location,
    outputDirectory
  )

  const options = {
    babelrc: false,
    comments: false,
    filename: location,
    minified: true,
    sourceMaps: true,
    presets: [
      require('babel-preset-babili')
    ]
  }

  const {code, map} = transform(data, options)
  await Promise.all([
    promisify(write)(
      outputPath,
      code + `\n//# sourceMappingURL=${encodeURI(basename(outputPath))}.map`
    ),
    promisify(write)(
      `${outputPath}.map`,
      JSON.stringify(map)
    )
  ])

  return {location, data, fingerprint, outputPath}
}

async function setupBootloader () {
  const location = join(__dirname, '..', 'src', 'bootloader.js')
  const outputPath = join(this.destination, basename(this.entry))
  const data = await promisify(readFile)(location)

  const patterns = {
    SERVICE_WORKER: slash('./' + relative(
      dirname(outputPath),
      this._serviceWorkerImmutableEntry.outputPath
    )),
    SYSTEM_JS: slash('./' + relative(
      dirname(outputPath),
      this._systemJsEntry.outputPath
    )),
    ENTRY: slash('./' + relative(
      dirname(outputPath),
      this._entryRevved.outputPath
    ))
  }

  const options = {
    babelrc: false,
    comments: false,
    filename: location,
    minified: true,
    sourceMaps: true,
    plugins: [
      [require('babel-plugin-transform-string-literal-replace'), {patterns}]
    ],
    presets: [
      require('babel-preset-babili')
    ]
  }

  const {code, map} = transform(data, options)
  await Promise.all([
    promisify(write)(
      outputPath,
      code + `\n//# sourceMappingURL=${encodeURI(basename(outputPath))}.map`
    ),
    promisify(write)(
      `${outputPath}.map`,
      JSON.stringify(map)
    )
  ])
}

export class Loader {
  constructor () {
    this.fingerprinter = new Fingerprinter()
  }

  async run (entry, destination, assets) {
    this.entry = entry
    this.destination = destination
    this._entryRevved = assets.find(({location}) => location === entry)

    const [serviceWorkerImmutableEntry, systemJsEntry] = await Promise.all([
      this::setupServiceWorkerImmutable(),
      this::setupSystemJs()
    ])

    this._serviceWorkerImmutableEntry = serviceWorkerImmutableEntry
    this._systemJsEntry = systemJsEntry

    await this::setupBootloader()
  }

  async expire (old, asset) {
    await this::setupBootloader()
  }
}
