import {Reporter} from './Reporter'

export class Verbose extends Reporter {
  constructor (promises, {log = console.log} = {}) {
    super(...arguments)
    this.log = log
  }

  resolved (promise, res) {
    super.resolved(promise, res)
    this.log(res)
  }
}
