import {watch} from 'chokidar'
import debounce from 'debounce-collect'

export class Watcher {
  constructor () {
    this._watcher = watch()
  }

  on (eventName, listener) {
    return this._watcher.on(eventName, debounce(listener, 50))
  }

  add (assets, options) {
    return this._watcher.add(
      assets.map((asset) => asset.location || asset),
      options
    )
  }

  unwatch (assets) {
    return this._watcher.unwatch(
      assets.map((asset) => asset.location || asset)
    )
  }

  kill () {
    this._watcher.close()
  }
}
