import {processFile} from './processFile'

// process.on('unhandledRejection', console.trace)

let cached

process.on('message', async ({type, payload}) => {
  if (type === 'cached') {
    cached = payload
    process.send({type: 'ready'})
  } else if (type === 'file') {
    let result
    try {
      result = await processFile(payload, cached)
    } catch (error) {
      process.send({type: 'error', payload: error})
      return
    }
    process.send({type: 'result', payload: result})
  }
})

process.send({type: 'ready'})
