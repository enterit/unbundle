<img alt="💠" width="256" height="256" style="display: block; margin: 128px auto;" src="https://cdn.rawgit.com/Ranks/emojione/master/assets/png_512x512/1f4a0.png">

# Unbundle 💠

Unbundle transforms CommonJS and ES2015 Modules syntax to load in the browser *without* bundling everything into a single file.

- Made for HTTP/2 Server Push. Eliminates the HTTP/1.x legacy bundling of Browserify/Webpack. Allows atomic caching of files as browsers are designed, not unseemly opaque blobs.
- Works with any HTTP/2, or indeed HTTP/1.x, static webserver or CDN.
- NPM support that brings `node_modules` into the browser.
- Parallel processing to scale with the number of CPU cores available.
- Out of the box support for ES2015+, JSX, and Flow using Babel.
- Incremental builds (`--watch`) to only rebuild what is needed, when it's needed.
- Cache busting with emoji-encoded file revving for Cache Digests.
- Compresses code using [Babili](https://github.com/babel/babili), an ES6-aware minifier based on Babel.
- Source maps are generated per file for convenient debugging.

## CLI

```
unbundle [options]
```

### Options

#### `-i`, `--entry` `<file>`

Starting file path for the application. Defaults to `package.json` `main` field.

#### `-o`, `--destination` `<directory>`

Directory path to write output. Defaults to `./public`.

Nonexistent directories are automatically created. 😌

Existent directories are overwritten. 😯

#### `-s`, `--silent`

Suppress log messages from output

#### `-w`, `--watch`

Monitor files and rebuild on changes

#### `-h`, `--help`

Show help

#### `-v`, `--version`

Show version number

### Examples

```
unbundle -i src/app.js -o dist
```

Transpiles the app and its dependencies into the "dist" directory.

## Tutorial

Let's create a new project.

```
mkdir ~/my-app
cd $_
```

Here's a contrived **Hello World** snippet using ES2015 modules and JSX syntax.

#### `app.js`

```jsx
import React from 'react'
import ReactDOM from 'react-dom'

class MyComponent extends React.Component {
  render () {
    return <div>Hello World</div>
  }
}

ReactDOM.render(<MyComponent />, document.body)
```

*JSX support is enabled by default and uses the `react` preset for Babel.*

We'll set some basic meta data for NPM. By default, if no `---entry` is specified, Unbundle looks for the entry point of our app in the `main` field of `package.json`.

#### `package.json`
```json
{
  "main": "app.js"
}
```

Use NPM to install the dependencies for the app.

```bash
npm install --save react react-dom
```

Not to worry, there is no *obligation* to use React. This is just an example. Any NPM modules are transpiled using the [transform-commonjs-es2015-modules](https://www.npmjs.com/package/babel-plugin-transform-commonjs-es2015-modules) Babel plugin.

*Unbundle runs multiple passes through Babel to transpile CommonJS and ES2015 modules into SystemJS so they can be loaded asynchronously in the browser.*

All we need now is an HTML file that launches the app.

#### `public/index.html`
```html
<script src="app.js"></script>
```

#### Let's do it!

Install and run the `unbundle` command:
```bash
npm install --save-dev unbundle
./node_modules/.bin/unbundle
```

Serve up the generated files using your favourite static webserver. Ideally, use a webserver with sufficiently intelligent HTTP/2 Server Push support. For example, [http2server](https://www.npmjs.com/package/http2server) or [H2O](https://h2o.examp1e.net).

```bash
npm install --global http2server
http2server -o
```

Behold! A browser opens to reveal your unbundled & pushed *Hello World* masterpiece.

Welcome to the future. 😎

## Colophon

Made by Sebastiaan Deckers at [Wessex Estate](https://en.wikipedia.org/wiki/My_Queenstown_Heritage_Trail) 🏡 in Singapore. 🇸🇬
