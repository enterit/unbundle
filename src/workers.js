import physicalCpuCount from 'physical-cpu-count'
import {fork} from 'child_process'
import {EventEmitter} from 'events'

class Worker extends EventEmitter {
  _status = 'starting'

  get status () {
    return this._status
  }

  set status (value) {
    this._status = value
    this.emit('status', value)
  }

  modulePath = require.resolve('./worker')

  constructor () {
    super()
    this.child = fork(this.modulePath, [], {
      env: process.env,
      cwd: process.cwd(),
      execArgv: []
    })
    this.child.on('message', ({type, payload}) => {
      if (type === 'ready') {
        this.status = 'idle'
      } else if (type === 'result' || type === 'error') {
        const resolve = this._resolve
        const reject = this._reject
        delete this._resolve
        delete this._reject
        this.status = 'idle'
        if (resolve && type === 'result') resolve(payload)
        else if (reject && type === 'error') reject(payload)
      }
    })
  }

  preload (cached) {
    this.status = 'loading'
    this.child.send({type: 'cached', payload: cached})
  }

  process (file) {
    const promise = new Promise((resolve, reject) => {
      this._resolve = resolve
      this._reject = reject
      this.child.send({type: 'file', payload: file})
    })
    this.status = 'busy'
    return promise.then(() => file)
  }

  kill () {
    this.child.kill('SIGTERM')
  }
}

export class Pool {
  constructor () {
    this.pending = new Queue()
    this.workers = Array(physicalCpuCount).fill().map(() => {
      const worker = new Worker()
      worker.on('status', (status) => {
        if (status === 'idle') {
          const job = this.pending.take()
          if (job) job.run(worker)
        }
      })
      return worker
    })
  }

  process (files, cached) {
    this.pending.clear()
    for (const worker of this.workers) {
      worker.preload(cached)
    }
    const jobs = []
    for (const file of files) {
      const job = new Job(async (worker) => await worker.process(file))
      jobs.push(job)

      const worker = this.workers.find((worker) => worker.status === 'idle')
      if (worker) job.run(worker)
      else this.pending.add(job)
    }
    return jobs
  }

  kill () {
    for (const worker of this.workers) worker.kill()
  }
}

class Job /* extends Promise */ {
  constructor (task) {
    this._promise = new Promise((resolve, reject) => {
      this.resolve = resolve
      this.reject = reject
    })

    this.task = task
  }

  run (worker) {
    return this.task(worker)
      .then(::this.resolve, ::this.reject)
  }

  cancel () {
    return this.reject(...arguments)
  }

  then () {
    return this._promise.then(...arguments)
  }

  catch () {
    return this._promise.catch(...arguments)
  }
}

class Queue {
  constructor () {
    this._buffer = []
  }

  add (value) {
    this._buffer.push(value)
  }

  take () {
    return this._buffer.shift()
  }

  clear () {
    const jobs = this._buffer
    this._buffer = []
    for (const job of jobs) {
      job.cancel()
    }
  }
}
