import GaugeLib from 'gauge'
import {Reporter} from './Reporter'

function cleanup () {
  this.gauge.disable()
  this.gauge.hide()
  clearInterval(this.pulse)
}

export class Gauge extends Reporter {
  constructor (promises, options) {
    super(promises, options)
    this.gauge = new GaugeLib({
      cleanupOnExit: false
    })
    this.gauge.pulse()
    this.pulse = setInterval(() => this.gauge.pulse(), 50)
    this.then(this::cleanup, this::cleanup)
  }

  resolved (promise, res) {
    super.resolved(promise, res)
    this.gauge.show(res, this.progress / this.total)
  }
}
