import test from 'ava'
import {processFile} from '../lib/processFile'
import {dir} from 'tmp'
import promisify from 'pify'
import {resolve} from 'path'
import {F_OK, access, readFile} from 'fs'

async function createDirectory () {
  return await promisify(dir, {multiArgs: true})({unsafeCleanup: true})
}

let cleanup
test.afterEach((t) => {
  if (cleanup) {
    cleanup()
    cleanup = undefined
  }
})

test('transpile into correct location', async (t) => {
  const location = resolve('fixtures/app.js')
  const data = `'hello world'`
  const [destination, clean] = await createDirectory()
  const outputPath = resolve(destination, 'app.js')
  cleanup = clean
  await processFile({location, outputPath, data}, [])
  await t.notThrows(Promise.all([
    promisify(access)(outputPath, F_OK),
    promisify(access)(outputPath + '.map', F_OK)
  ]))
  const code = await promisify(readFile)(outputPath, 'utf8')
  t.truthy(/\/\/# sourceMappingURL=app.js.map$/.test(code))
  t.pass()
})
