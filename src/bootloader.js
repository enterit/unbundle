/* eslint-env browser */
/* global SystemRegisterLoader */

(function (global) {
  const script = document.createElement('script')
  script.async = true
  script.src = 'SYSTEM_JS'
  script.addEventListener('load', () => {
    // Create a global SystemJS instance for the modules to call `System.register`.
    global.System = new SystemRegisterLoader()
    System.import('ENTRY')

    // Wait until everything else on the page has loaded since the service worker
    // only receives `fetch` events on future page loads.
    if ('serviceWorker' in navigator) {
      navigator.serviceWorker.register('SERVICE_WORKER', {scope: '/'})
    }

    // Clean up after ourselves so it looks a bit more magical.
    script.parentNode.removeChild(script)
  }, {once: true})

  // Safest way to inject a script. Trust no one.
  document.currentScript.parentNode.insertBefore(
    script,
    document.currentScript
  )
})(this)
